from bokeh.plotting import figure, show, output_file
from bokeh.sampledata.us_counties import data as counties
from bokeh.sampledata.us_states import data as states
import pandas as pd


# Reads original medicare csv into dataframe
# with index.
original = pd.read_csv(
    '../original/Medicare_Provider_Charge_Outpatient_APC32_CY2014.csv')
original.index.name = 'ID'


print original


# Reads ordered georesults into dataframe.
results = pd.read_csv('./Ordered_Results/georesults_ordered_new.csv')

# Merges original csv with georesults
# csv.
master_file = original.join(results, how='inner')

# Combine State and County into tuple.
master_file['FIPS'] = zip(master_file.State, master_file.County)

# MultiIndex dataframe
index = pd.MultiIndex.from_tuples(master_file['FIPS'],
                                  names=['first', 'second'])
# Set FIPS tuple to index and sort rows
# by FIPS
master_file = master_file.set_index(index)

# master_file.index.name = 'FIPS'
master_file = master_file.sort_index(axis=0, level=None, ascending=True,
                                     inplace=False, kind='quicksort',
                                     na_position='last',
                                     sort_remaining=True,
                                     by=None)

print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
print master_file
print "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"

# Map
del states["HI"]
del states["AK"]

state_xs = [states[code]["lons"] for code in states]
state_ys = [states[code]["lats"] for code in states]

county_xs = [counties[code]["lons"] for code in counties if counties[code]
             ["state"] not in ["ak", "hi", "pr", "gu", "vi", "mp", "as"]]

county_ys = [counties[code]["lats"] for code in counties
             if counties[code]["state"]
             not in ["ak", "hi", "pr", "gu", "vi", "mp", "as"]]


colors = ["#5375B9", "#5063B", "#4D52A9", "#524BA1", "#5A4899",
          "#604592", "#66428A", "#6A3F82", "#6C3C7A", "#6E3973"]

county_colors = []

print "##################"

# Test code
count = 0
avg = []
for county_id in counties:
    count += 1
    try:
        avg.append(master_file.loc[county_id].Average_Total_Payments.mean())
    except:
        continue
print avg
print "Length: ", len(avg)
print "Number of counties ", count
quant = pd.qcut(avg, 10, retbins=True,
                labels=colors)
color_bins = quant[1]
print color_bins
# end Test code

# Old code

'''
quant = pd.qcut(master_file["Average_Total_Payments"], 10, retbins=True,
                labels=colors)
color_bins = quant[1]
print color_bins


print "*********************************************"
'''

for county_id in counties:
    if counties[county_id]["state"] in \
       ["ak", "hi", "pr", "gu", "vi", "mp", "as"]:
        continue

    try:
        mean = master_file.loc[county_id].Average_Total_Payments.mean()
        bin = 0
        while mean > color_bins[bin]:
            bin += 1

        print county_id, mean, bin
        county_colors.append(colors[bin])

    except:
        county_colors.append("black")

p = figure(title="Average Total Payments", toolbar_location="left",
           plot_width=1100, plot_height=700)

p.patches(county_xs, county_ys,
          fill_color=county_colors, fill_alpha=0.7,
          line_color="white", line_width=0.5)

p.patches(state_xs, state_ys, fill_alpha=0.0,
          line_color="#884444", line_width=2, line_alpha=0.3)

output_file("Medicare.html", title="Medicare.py example")

show(p)
