#!/bin/sh
# geocoder - Requests FIPS code data for address batches from census.gov

echo "ID,Input_Address,Match,Match_Type,Matched_Address,Coordinates,Tiger_Line_Id,Side,State,County,11,12" > \
     ~/Projects/bokeh_project/Medicare/geocoderesult.csv


# Array of Medicare_Addresses_*.csv file numbers in order.
fileArray=(0 500 1000 1500 2000 2500 3000 3500 4000 4500 5000 5500 6000 6500 7000 7500 8000 8500 \
             9000 9500 10000 10500 11000 11500 12000 12500 13000 13500 14000 14500 15000 15500 16000 \
             16500 17000 17500 18000 18500 19000 19500 20000 20500 21000 21500 22000 22500 23000 23500 \
             24000 24500 25000 25500 26000 26500 27000 27500 28000 28500 29000 29500 30000 30500 31000 \
             31500 32000 32500 33000 33500 34000 34500 35000 35500)

for i in "${fileArray[@]}";
do
    file="$HOME/Projects/bokeh_project/Medicare/address_batches/Medicare_Addresses_${i}.csv"
    echo "Working on: " $file
    curl -v --form addressFile=@$file \
         --form benchmark=Public_AR_Current \
         --form vintage=Current_Current \
         https://geocoding.geo.census.gov/geocoder/geographies/addressbatch >> \
         ~/Projects/bokeh_project/Medicare/geocoderesult.csv
done
