import pandas as pd


# Reads geocoderesults to a dataframe in chunks with
# ID as index.
iter_results = pd.read_csv('../geocoderesult.csv', index_col=['ID'],
                           iterator=True, chunksize=100)

# Rolls up geocoderesult chunks into one dataframe.
results = pd.concat(iter_results, ignore_index=False)

# Sorts geocoderesults dataframe by ID number
# and writes the dataframe to a csv.
results = results.sort_index(axis=0, level=None, ascending=True,
                             inplace=False, kind='quicksort',
                             na_position='last', sort_remaining=True, by=None)

results.to_csv('./Ordered_Results/georesults_ordered.csv', na_rep='NaN')
