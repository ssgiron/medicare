import pandas as pd
import requests
import geocoder
import os.path
import signal
import time
import sys


def signal_handler(signal, frame):
    print('You pressed Ctrl+C')
    codes.to_csv("./georesults_ordered_new.csv", index=False, na_rep='NaN')
    sys.exit(0)


# Reads georesults_ordered.csv into a dataframe.
if os.path.exists("./georesults_ordered_new.csv"):
    codes = pd.read_csv("./georesults_ordered_new.csv")

if os.path.exists("./georesults_ordered_final.csv"):
    print "add_codes.py completed"
    exit()

if (os.path.exists("./georesults_ordered_new.csv") is False):
    codes = pd.read_csv("./georesults_ordered.csv")

idc = 0
searched = 0
status_code = 'OK'

for line in codes.Match:

    # Find a line in which FIPS could not be matched
    # for the given address.
    if line == 'No_Match':
        print "Lines remaining: ", (len(codes.Match) - idc)
        print ">>>>>>>", "Match: ", line, idc, "<<<<<<<"
        addr = codes.loc[idc].Input_Address

        # Geocode input address with google.
        geo = geocoder.google(addr,
                              key='AIzaSyCx3U0loa0aw0YRo39bM2AQISpzAdDvVjY')

        # When over the 2500/day query limit
        # write codes dataframe to csv.
        # *** switch statement here ***

        if geo.status == 'ZERO_RESULTS':
            print geo.status
            geo = geocoder.mapquest(addr,
                                    key='prNlBxHu40cUA2M7A65iuvqbkLArAy4S')
            if geo.status != 'OK':
                print geo.status, 'No Results'
                idc += 1
                continue

        if geo.status == 'OVER_QUERY_LIMIT':
            print "over limit"
            time.sleep(1)
            geo = geocoder.google(
                addr, key='AIzaSyCx3U0loa0aw0YRo39bM2AQISpzAdDvVjY')

            if geo.status == 'OVER_QUERY_LIMIT':
                status_code = geo.status
                break

        if geo.status == 'REQUEST_DENIED':
            print geo.status
            geo = geocoder.mapquest(addr,
                                    key='prNlBxHu40cUA2M7A65iuvqbkLArAy4S')
            if geo.status != 'OK':
                print geo.status, 'No Results'
                idc += 1
                continue

        if geo.status == 'INVALID_REQUEST':
            print geo.status
            geo = geocoder.mapquest(addr,
                                    key='prNlBxHu40cUA2M7A65iuvqbkLArAy4S')
            if geo.status != 'OK':
                print geo.status, 'No Results'
                idc += 1
                continue

        if geo.status == 'UNKNOWN_ERROR':
            print geo.status
            geo = geocoder.mapquest(addr,
                                    key='prNlBxHu40cUA2M7A65iuvqbkLArAy4S')
            if geo.status != 'OK':
                print geo.status, 'No Results'
                idc += 1
                continue

        # Prepare parameters for census block request.
        payload = {'latitude': geo.lat, 'longitude': geo.lng,
                   'showall': 'true', 'format': 'json'}

        # Request with given latitude and longitude
        # and return a json containing FIPS.
        r = requests.get('http://data.fcc.gov/api/block/find?', params=payload)

        # Insert FIPS codes in dataframe
        fips = (r.json()['County']['FIPS'])
        print fips
        if (fips != 'None'):
            fips = str(fips)
            state = float(fips[0:2])
            county = float(fips[2:5])
            codes.loc[idc:idc, 'State'] = state
            codes.loc[idc:idc, 'County'] = county
            codes.loc[idc:idc, 'Match'] = 'Match'
            print codes.loc[idc]
            signal.signal(signal.SIGINT, signal_handler)

        # Increment the number of searches.
        searched += 1

    # Increment the ID to get the next line in
    # the codes dataframe.
    idc += 1

if status_code == 'OK':
    print "completed"
    codes.to_csv("./georesults_ordered_final.csv", index=False, na_rep='NaN')

print status_code
codes.to_csv("./georesults_ordered_new.csv", index=False, na_rep='NaN')
