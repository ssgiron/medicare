# get_Address - Extracts address information from Medicare csv

import pandas as pd

# Reads original csv file into a dataframe.
# Pandas implicitly adds indexes.
medData = pd.read_csv(
    '../original/Medicare_Provider_Charge_Outpatient_APC32_CY2014.csv')

# Creates new dataframe with address data.
addr = medData[['Provider_Street_Address', 'Provider_City', 'Provider_State',
                'Provider_Zip_Code']]

# Writes addresses to a csv file.
addr.to_csv('../addresses/Medicare_Addresses.csv')

# Writes indexed Medicare data to a csv file.
medData.to_csv('../indexed/Indexed_Medicare_Provider_Data.csv')
